﻿FROM wordpress:6.2

RUN pecl install redis \
&& pecl list | grep redis | awk -F " " '{print $1 " " $2}'